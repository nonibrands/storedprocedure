USE [TRL_DB2015]
GO
/****** Object:  StoredProcedure [dbo].[spChitChats_EndOfDay_Select]    Script Date: 12/2/2020 12:57:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spChitChats_EndOfDay_Select] 
	-- Add the parameters for the stored procedure here
	 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT o.orderid
		, o.ocurrency
		, o.totalvolume
		, oshipmethodtype AS ShippingMethod
		, producttotal AS prodtotal
		, o.totalorderweight
		, CASE WHEN LEN(oshipname) > 0 
				THEN oshipaddress 
				ELSE oaddress 
			END AS addy
		, ISNULL(oshipname, ofirstname + ' ' + olastname) AS name
		, o.ofirstname
		, o.olastname
		, CASE WHEN LEN(oshiptown) > 0 
				THEN oshiptown 
				ELSE ocity 
			END AS city
		, CASE WHEN ISNULL(oshipstate, ostate) = 'none' 
				THEN '' 
				ELSE 
					CASE WHEN LEN(oshipname) > 0 
							THEN oshipstate 
							ELSE ostate 
						END 
			END AS prov
		, CASE WHEN LEN(oshipname) > 0 
				THEN oshipzip 
				ELSE opostcode 
			END AS postcode
		, CASE WHEN LEN(oshipname) > 0 
				THEN oshipcountry 
				ELSE ocountry 
			END AS Country
		, 'Craft Supplies - parts used in making jewelry and craft' AS Content_Desc 

		, ISNULL(oshipemail, oemail) AS email
		, CASE WHEN LEN(oshipname) > 0 
				THEN oshipaddress2 
				ELSE oaddress2 
			END AS addy2
		, CASE WHEN LEN(oshipname) > 0 
				THEN 
					CASE WHEN oshipcompany IS NOT NULL 
						THEN oshipcompany 
						ELSE oshipname 
					END 
				ELSE 
					CASE WHEN LEN(ocompany) > 0 
						THEN ocompany 
						ELSE ofirstname + ' ' + olastname 
					END 
			END AS company
		, o.upstrackno
		, CASE WHEN [ShipmentID] = 'NULL'
				THEN NULL
				ELSE [ShipmentID]
				END AS [ShipmentID]
		, CASE WHEN [ShipmentID] = 'NULL'
				THEN NULL
				ELSE o.ChitChatsLabelURL
				END AS ChitChatsLabelURL 
		, REPLACE(
			REPLACE(
				'{ "name": "' +  REPLACE(CASE WHEN LEN(RTRIM(LTRIM(ISNULL(oshipname, '')))) > 0 THEN oshipname
											ELSE ofirstname + ' ' + olastname
											END, '"', '')  + '", '
					+ '"address_1": "' +  REPLACE(CASE WHEN LEN(oshipaddress) > 0 THEN oshipaddress ELSE oaddress END, '"', '')  + '", '
					+  '"city": "' + CASE WHEN LEN(oshiptown) > 0 THEN oshiptown ELSE ocity END + '", '
					+  CASE WHEN ISNULL(oshipstate, ostate) = 'none' THEN '"province_code": "  ", '
													ELSE CASE WHEN LEN(LTRIM(RTRIM((oshipstate)))) > 0 AND NOT oshipstate = 'none'  THEN '"province_code": "' +  oshipstate + '", '
													ELSE CASE WHEN  LEN(LTRIM(RTRIM((ostate)))) > 0 AND NOT ostate = 'none' THEN '"province_code": "' +  ostate + '", '
													ELSE '"province_code": "  ", ' END END END 
					+  '"postal_code": "' + CASE WHEN LEN(oshipzip) > 0 THEN oshipzip ELSE opostcode END + '", '
					+  '"country_code": "' + CASE WHEN LEN(oshipcountry) > 0 THEN oshipcountry ELSE ocountry END + '", '
					+  '"description": "Craft Supplies - parts used in making jewlery and craft", '
					+  '"value": "' +  CAST(CASE WHEN ISNULL(producttotal, 0) <= 1.00 THEN 1.00 ELSE producttotal END  AS VARCHAR) + '", '
					+  '"value_unit": "usd", '
					+  '"package_type": "' + CASE WHEN oshipmethodtype LIKE '%Large Flat Rate Box%' THEN 'large_flat_rate_box'
													WHEN oshipmethodtype LIKE '%Medium Flat Rate Box%' THEN 'medium_flat_rate_box_1'
													 ELSE 'thick_envelope' END + '", '
					+  '"weight": "' + ISNULL(CAST(CASE WHEN o.totalorderweight <> 0 
														THEN o.totalorderweight
														ELSE ISNULL(o.ShippedWt, 0)
														END  AS VARCHAR), '')+ '", '
					+  '"weight_unit": "lb", '
					+  '"postage_type": "' + CASE WHEN oshipmethodtype = 'International Post Shipping' THEN 'chit_chats_international_not_tracked'
													 WHEN oshipmethodtype = 'USPS First-Class Package' THEN 'usps_first'
													 WHEN oshipmethodtype = 'USPS First-Class Package International' THEN 'usps_first_class_package_international_service'
													 WHEN oshipmethodtype = 'USPS Priority Mail International - Large Flat Rate Box' THEN 'usps_priority_mail_international'
													 WHEN oshipmethodtype = 'USPS Priority Mail International - Medium Flat Rate Box' THEN 'usps_priority_mail_international'
													 WHEN oshipmethodtype = 'USPS Priority Mail International - Padded Flat Rate Envelope' THEN 'usps_priority_mail_international'
													 WHEN oshipmethodtype = 'USPS Priority Mail International Package' THEN 'usps_priority_mail_international'
													 WHEN oshipmethodtype = 'USPS Priority Mail International Package/Flat/Thick Envelope' THEN 'usps_priority_mail_international'
											 ELSE 'usps_priority' END + '", '
					+ CASE WHEN oshipmethodtype = 'International Post Shipping' THEN '"ship_date": "today", "size_unit": "in", "size_x":"9.5", "size_y":"7.2", "size_z":"0.2"'
							ELSE '"tracking_code": "' + ISNULL(upstrackno, '') +'"' END 
					+  + '} '
			, CHAR(13), '')
			, CHAR(10), '')  AS ChitChatString
	FROM orders o
	WHERE (oshipmethodtype LIKE 'USPS%' 
			OR oshipmethodtype LIKE 'International%' ) 
	AND (oprocessed = '1') 
	AND (opending = '-1') 
	AND ostatus ='printed'

END 